Coda Express Starter Application
=========================

# Installation

Clone the repository

```
npm install 
```

For development:
```
node server.js
```

Browse to http://localhost:3000
